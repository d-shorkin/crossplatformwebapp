// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import 'vuetify/dist/vuetify.min.css'

import './Styles/main.scss'

import Vuetify from 'vuetify'

Vue.use(Vuetify);




/**
 * Config
 */

import config from "./config"

/**
 * Vuex
 */

import store from './Store'


/**
 * Notifications
 */

import NotifyPlugin from './Plugins/NotifyPlugin';

Vue.use(NotifyPlugin);

/**
 * Request library
 */

import ApiRequest from './Plugins/ApiPlugin';

Vue.use(ApiRequest, {baseUrl: config.api_base_url});

/**
 * StaticHelper
 */

import StaticHelper from './Plugins/StaticHelper';

Vue.use(StaticHelper);




Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    template: '<App/>',
    components: {App},
    store
});
