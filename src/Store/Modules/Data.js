import Vue from 'vue';

export default {
    namespaced: true,
    state: {
        ready   : false,
        data    : {},
    },
    mutations: {
        SET_DATA(state, {meta, shifts, data}){

            let peaks = ['13:00', '13:30', '14:00', '14:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30'];

            try{
                let final = { total: {} , peaks};

                if (!shifts || typeof shifts.manager == "undefined"){
                    return;
                }

                // Add index props for all managers
                let tmpManagers = shifts.manager;
                tmpManagers.forEach((item, index) => { item.index = index; });

                // Sort by manager 'to' property
                tmpManagers.sort((a, b)=>{ return (a.to % 24) - (b.to % 24)});

                final.managers = tmpManagers.reduce((obj, manager, index) => {

                    let i = manager.index;
                    obj[index] = {};

                    let current = obj[index];

                    current.manager = {
                        name    : ( manager.manager != 'Нет данных') ? manager.manager : null,
                        rating  : parseInt(shifts.rating[i].rating) || 0,
                        rating_letter  : shifts.ratingLetter[i].rating_letter,
                        shifts  : shifts.shifts[i]
                    };

                    // Delete last element from shifts
                    current.manager.shifts.pop();

                    current.total = {};

                    current.peaks = {};

                    current.data = meta.reduce((obj, meta)=>{

                        if(typeof final.total[meta.name] === "undefined"){
                            final.total[meta.name] = {plan: 0, fact: 0};
                        }

                        if(typeof current.total[meta.name] === "undefined"){
                            current.total[meta.name] = {plan: 0, fact: 0};
                        }

                        if(typeof current.peaks[meta.name] === "undefined"){
                            current.peaks[meta.name] = {plan: 0, fact: 0};
                        }

                        obj[meta.name] = current.manager.shifts.reduce((arr, time)=>{
                            if(typeof data[meta.name] === "undefined" || typeof data[meta.name][time] === "undefined"){
                                arr.push({plan: 0, fact: 0, time: time});
                                return arr;
                            }

                            arr.push({
                                plan: parseFloat(data[meta.name][time].plan) || 0,
                                fact: parseFloat(data[meta.name][time].fact) || 0,
                                time: time
                            });

                            final.total[meta.name].plan += arr[arr.length - 1].plan;
                            final.total[meta.name].fact += arr[arr.length - 1].fact;

                            current.total[meta.name].plan += arr[arr.length - 1].plan;
                            current.total[meta.name].fact += arr[arr.length - 1].fact;

                            if(peaks.indexOf(time) + 1){
                                current.peaks[meta.name].plan += arr[arr.length - 1].plan;
                                current.peaks[meta.name].fact += arr[arr.length - 1].fact;
                            }

                            return arr;

                        }, []);

                        return obj;
                    }, {});

                    return obj;

                }, {});

                state.data = final;
                state.ready = true;
            }catch (e){
                Vue.alert.error('Ошибка в данных сервера');
                console.error(e);
            }
        },
        CLEAR_DATA(state){
            state.ready = false;
            state.data = {};
        }
    },
    actions: {
        reload({commit}){

            let promises = [];

            promises.push( Vue.api.get("meta") );

            promises.push( Vue.api.get("shifts") );

            promises.push( Vue.api.get("data") );

            return Promise.all(promises)
                .then(values => {
                    commit("SET_DATA", {meta: values[0].data, shifts: values[1].data, data: values[2].data});
                    return true
                })
        },
        clear({commit}){
            commit("CLEAR_DATA");
        }
    }
};