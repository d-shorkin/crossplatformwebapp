import Vue from 'vue';

export default {
    namespaced: true,
    state: {
        queue   : [],
        queueId : 0
    },
    mutations: {
        ADD_MSG(state, {message, type}) {
            Vue.set(state.queue, state.queueId ,  {message, type, id: state.queueId});
            state.queueId++;
        },
        REMOVE_MSG(state, id){
            Vue.set(state.queue, id , null);
        }
    },
    getters: {
        notifications: state => {
            return state.queue.filter(n => (typeof n !== "undefined" && n != null))
        }
    },
    actions: {
        info({ dispatch }, message){
            dispatch("_addMsg", {message, type: 'info'});
        },
        error({ dispatch }, message){
            dispatch("_addMsg", {message, type: 'error'});
        },
        _addMsg({ commit, state }, messageObj){
            let id = state.queueId;
            commit("ADD_MSG", messageObj);
            setTimeout(()=>{
                commit("REMOVE_MSG", id);
            }, 10000);
        }
    }
};