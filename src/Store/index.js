import Vue from 'vue';
import Vuex from 'vuex';

//--- Modules ---
import data from './Modules/Data';
import notify from './Modules/Notify';


Vue.use(Vuex);

export default new Vuex.Store({
    modules: { data , notify}
});