const ApiPlugin = (Vue, options) => {

    const axios = require('axios');

    const localStorage = require('localStorage');

    let axiosInstance = axios.create({ baseURL: options.baseUrl });

    axiosInstance.interceptors.request.use(config =>  {
        config.headers = { Accept: 'application/json'};

        if(localStorage.getItem("token")){
            config.headers.auth = localStorage.getItem("token");
        }

        return config;
    });


    function errorHandler(err) {
        return new Promise( (resolve, reject) => {

            if( !err.response || !err.response.data || err.response.data.success !== false ){

                Vue.alert.error(err.message);

                return reject(err);

            }

            let data = err.response.data;

            if(!data.message) {

                Vue.alert.error(err.message);

                return reject(err);

            }

            Vue.alert.error(data.message);

            return reject(err);
        });

    }


    axiosInstance.interceptors.response.use(
        (result) => {
            let data = result.data;

            if(data.success){
                return data;
            }


            let err = new Error("Something wrong");

            err.response = result;

            return errorHandler(err);
        },
        (error) => {
            return errorHandler(error);
        }
    );


    Vue.prototype.$api = Vue.api = axiosInstance;
};

export default {install: ApiPlugin};