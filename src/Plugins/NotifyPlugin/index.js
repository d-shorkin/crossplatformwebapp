import store from './../../Store';


const AlertPlugin = (Vue) => {
    Vue.prototype.$alert = Vue.alert = {
        info(message){
            store.dispatch("notify/info", message);
        },
        error(message){
            store.dispatch("notify/error", message);
        }
    };
};

export default {install: AlertPlugin};