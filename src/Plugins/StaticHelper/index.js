import config from "../../config";

const Plugin = (Vue) => {

    Vue.prototype.$static = Vue.static = (url)=>{
        return config.server_url.replace(/\/+$/,'') + "/" + url
    };
};

export default {install: Plugin};